package com.namelessdev.remotecontrol;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

public class mouseFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.mouse_view, container, false);

        FrameLayout pad = (FrameLayout)view.findViewById(R.id.mousepad);
        pad.setOnTouchListener(new TouchpadHandler());

        return view;
    }

}
