package com.namelessdev.remotecontrol;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.Button;

public class KeycodeButton extends Button {

    private String keycode;
    private String text_normal;
    private String text_alt;

    public int getKeycode() {
        return Integer.decode(keycode);
    }

    public String getNormalText() {
        if (this.text_normal == null) {
            return "";
        } else {
            return this.text_normal;
        }
    }

    public String getAltText() {
        if (this.text_alt == null) {
            return "";
        } else {
            return this.text_alt;
        }
    }

    public KeycodeButton (Context context) {
        super(context);
    }

    public KeycodeButton (Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public KeycodeButton (Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        TypedArray a=getContext().obtainStyledAttributes(attrs, R.styleable.KeycodeButtonStyle);
        this.keycode = a.getString(R.styleable.KeycodeButtonStyle_keycode);
        this.text_normal = this.getText().toString();
        this.text_alt = a.getString(R.styleable.KeycodeButtonStyle_text_alt);
        a.recycle();
    }
}
