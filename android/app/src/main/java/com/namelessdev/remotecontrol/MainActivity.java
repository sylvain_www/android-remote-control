package com.namelessdev.remotecontrol;

import com.rekap.network.NetInput;
import com.rekap.network.Network;

import android.os.Bundle;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.content.Intent;

public class MainActivity extends Activity {

    private int TAG_SHIFT =  0x10000;
    private int TAG_CTRL  =  0x20000;
    private int TAG_META  =  0x40000;
    private int TAG_ALT   =  0x80000;
    private int TAG_ALTGR = 0x100000;
    private int TAG_FN    = 0x200000;
    private int TAG_CAPSL = 0x400000;
    private int flags;
    private boolean mouseMove;
    private boolean mouseResize;

    public void resetToggles() {
        if (this.mouseMove) {
            NetInput.SendKeyUp(0x85);
            NetInput.LeftUp();
            this.mouseMove = false;
        }
        if (this.mouseResize) {
            NetInput.SendKeyUp(0x85);
            NetInput.RightUp();
            this.mouseResize = false;
        }
    }

    public void toggleMove(View view) {
        if (this.mouseResize) {
            resetToggles();
        }
        if (this.mouseMove) {
            NetInput.SendKeyUp(0x85);
            NetInput.LeftUp();
            this.mouseMove = false;
        } else {
            NetInput.SendKeyDown(0x85);
            NetInput.LeftDown();
            this.mouseMove = true;
        }
    }

    public void toggleResize(View view) {
        if (this.mouseMove) {
            resetToggles();
        }
        if (this.mouseResize) {
            NetInput.SendKeyUp(0x85);
            NetInput.RightUp();
            this.mouseResize = false;
        } else {
            NetInput.SendKeyDown(0x85);
            NetInput.RightDown();
            this.mouseResize = true;
        }
    }

    public void leftClick(View view) {
        resetToggles();
        NetInput.LeftClick();
    }

    public void rightClick(View view) {
        resetToggles();
        NetInput.RightClick();
    }

    public void middleClick(View view) {
        resetToggles();
        NetInput.MiddleClick();
    }

    public void scrollUp(View view) {
        resetToggles();
        NetInput.ScrollUp();
    }

    public void scrollDown(View view) {
        resetToggles();
        NetInput.ScrollDown();
    }

    public void backward(View view) {
        resetToggles();
        NetInput.GoPrev();
    }

    public void forward(View view) {
        resetToggles();
        NetInput.GoNext();
    }

    public void sendkeycode(View view) {

        KeycodeButton keycodebutton = (KeycodeButton)view;
        int keycode = keycodebutton.getKeycode();
        LinearLayout rootView = (LinearLayout) findViewById(R.id.keyboard);

        if (keycode == 0x32 || keycode == 0x3e) {
            // shift key
            this.flags ^= TAG_SHIFT;
            applyLabels(rootView);
        } else if (keycode == 0x25) {
            // ctrl key
            this.flags ^= TAG_CTRL;
        } else if (keycode == 0x85) {
            // meta key
            this.flags ^= TAG_META;
        } else if (keycode == 0x40) {
            // alt key
            this.flags ^= TAG_ALT;
        } else if (keycode == 0x6c) {
            // altgr key
            this.flags ^= TAG_ALTGR;
        } else if (keycode == 0x42) {
            // capslock key
        } else if (keycode == 0xff) {
            // function key
        } else {
            if (this.flags != 0) {
                keycode = keycode | this.flags;
                this.flags = 0;
                applyLabels(rootView);
            }
            NetInput.SendKeycode(keycode);
        }
    }

    public void launchPlayerApp(View view) {
        Intent LaunchIntent = getPackageManager().getLaunchIntentForPackage("com.namelessdev.mpdroid");
        startActivity(LaunchIntent);
    }

    private void applyLabels(LinearLayout parent)
    {
        for(int i = 0; i < parent.getChildCount(); i++)
        {
            View child = parent.getChildAt(i);
            if(child instanceof LinearLayout) {
                applyLabels((LinearLayout)child);
            }
            else if(child != null) {
                if ((this.flags & TAG_SHIFT) != 0) {
                    ((KeycodeButton)child).setText(((KeycodeButton)child).getAltText());
                } else {
                    ((KeycodeButton)child).setText(((KeycodeButton)child).getNormalText().toString());
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            hotkeysFragment firstFragment = new hotkeysFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            firstFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, firstFragment).commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.flags = 0;
        this.mouseMove = false;
        this.mouseResize = false;
        Network.Connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Network.CloseConnection();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            View decorView = getWindow().getDecorView();
            // Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        } else {
            resetToggles();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void displayMouse(View view) {
        resetToggles();
        mouseFragment newFragment = new mouseFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment);
        transaction.commit();
    }

    public void displayKeyboard(View view) {
        resetToggles();
        keyboardFragment newFragment = new keyboardFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment);
        transaction.commit();
    }

    public void displayHotkeys(View view) {
        resetToggles();
        hotkeysFragment newFragment = new hotkeysFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment);
        transaction.commit();
    }

}
