package com.rekap.network;

import com.protoscratch.client.ScratchClient;
import com.protoscratch.common.ScratchEvents;
import com.protoscratch.common.ScratchMessage;

public class Network extends ScratchEvents {

    private static ScratchEvents client;
    private static long lastConnect;

    public synchronized static ScratchEvents getClient()
    {
        return client;
    }

    public synchronized static boolean isConnected()
    {
        return client != null;
    }

    public synchronized static void Connect()
    {
        CloseConnection();
        if (client != null) return;
        long thisTime = System.currentTimeMillis();
        if (thisTime - lastConnect > 5000)
        {
            lastConnect = thisTime;
            ScratchClient client = new ScratchClient(Network.class, 28532);
            client.setKey(0x3FB3BFEF);
            client.setVersion((byte)1,(byte)0);
            client.Connect("192.168.0.100");
        }
    }

    public synchronized static void CloseConnection()
    {
        if (client != null)
        {
            client.Close();
            client = null;
        }
    }

    @Override
    public void NewConnection() {
        client = this;
    }

    @Override
    public void MessageReceived(ScratchMessage Message) {
    }

    @Override
    public void ConnectionLost() {
        client = null;
    }

}
