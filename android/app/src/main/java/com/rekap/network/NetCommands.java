package com.rekap.network;

public class NetCommands {
    public static final short Mouse = 10, Keyboard = 20;

    public static final short Move = 1, LeftClick = 2, MiddleClick=3, RightClick = 4, LeftDown = 5,
            LeftUp = 6, RightDown = 7, RightUp = 8, MiddleDown = 9, MiddleUp = 10,
            ScrollDown = 11, ScrollUp = 12, GoPrev=13, GoNext=14;

    public static final short SendKey = 1, SendKeyDown = 2, SendKeyUp = 3;

}
