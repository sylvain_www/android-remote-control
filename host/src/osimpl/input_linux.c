#include <X11/extensions/XTest.h>
#define META1 0x00010000 // shift
#define META2 0x00020000 // ctrl
#define META3 0x00040000 // meta
#define META4 0x00080000 // alt
#define META5 0x00100000 // altgr
#define KEYCODE_MASK 0xFF

static inline void SendKeyUp(int keyCode) {
    Display *display = XOpenDisplay(NULL);
    unsigned int XKeycode = keyCode & KEYCODE_MASK;
    if (display != NULL) {
        XTestFakeKeyEvent(display, XKeycode, False, CurrentTime);
        XFlush(display);
        XCloseDisplay(display);
    }
}

static inline void SendKeyDown(int keyCode) {
    Display *display = XOpenDisplay(NULL);
    unsigned int XKeycode = keyCode & KEYCODE_MASK;
    if (display != NULL) {
        XTestFakeKeyEvent(display, XKeycode, True, CurrentTime);
        XFlush(display);
        XCloseDisplay(display);
    }
}

static inline void SendKeycode(int keyCode) {
    Display *display = XOpenDisplay(NULL);
    unsigned int XKeycode = keyCode & KEYCODE_MASK;
    if (display != NULL) {
        if (keyCode & META1) {
            XTestFakeKeyEvent(display, 50, True, CurrentTime);
        }
        if (keyCode & META2) {
            XTestFakeKeyEvent(display, 37, True, CurrentTime);
        }
        if (keyCode & META3) {
            XTestFakeKeyEvent(display, 133, True, CurrentTime);
        }
        if (keyCode & META4) {
            XTestFakeKeyEvent(display, 64, True, CurrentTime);
        }
        if (keyCode & META5) {
            XTestFakeKeyEvent(display, 108, True, CurrentTime);
        }
        XTestFakeKeyEvent(display, XKeycode, True, CurrentTime);
        XTestFakeKeyEvent(display, XKeycode, False, CurrentTime);
        if (keyCode & META1) {
            XTestFakeKeyEvent(display, 50, False, CurrentTime);
        }
        if (keyCode & META2) {
            XTestFakeKeyEvent(display, 37, False, CurrentTime);
        }
        if (keyCode & META3) {
            XTestFakeKeyEvent(display, 133, False, CurrentTime);
        }
        if (keyCode & META4) {
            XTestFakeKeyEvent(display, 64, False, CurrentTime);
        }
        if (keyCode & META5) {
            XTestFakeKeyEvent(display, 108, False, CurrentTime);
        }
        XFlush(display);
        XCloseDisplay(display);
    }
}

static inline void SendClick(int button, Bool down) {
    Display *display = XOpenDisplay(NULL);
    if (display != NULL) {
        XTestFakeButtonEvent(display, button, down, CurrentTime);
        XFlush(display);
        XCloseDisplay(display);
    }
}

static inline void SendMouseMove(int X, int Y) {
    Display *display = XOpenDisplay(NULL);
    if (display != NULL) {
        XTestFakeRelativeMotionEvent(display, X, Y, CurrentTime);
        XFlush(display);
        XCloseDisplay(display);
    }
}
