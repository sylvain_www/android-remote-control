// +build linux

package osimpl

//#cgo LDFLAGS: -lX11 -lXtst
//#include "input_linux.c"
import "C"

func SendKeycode(keyCode int) {
    C.SendKeycode(C.int(keyCode))
}

func SendKeyUp(keyCode int) {
    C.SendKeyUp(C.int(keyCode))
}

func SendKeyDown(keyCode int) {
    C.SendKeyDown(C.int(keyCode))
}

func MoveMouse(X, Y int) {
    C.SendMouseMove(C.int(X), C.int(Y))
}

func LeftClick() {
    LeftDown()
    LeftUp()
}

func MiddleClick() {
    MiddleDown()
    MiddleUp()
}

func RightClick() {
    RightDown()
    RightUp()
}

func LeftDown() {
    C.SendClick(1, 1)
}

func RightDown() {
    C.SendClick(3, 1)
}

func LeftUp() {
    C.SendClick(1, 0)
}

func RightUp() {
    C.SendClick(3, 0)
}

func MiddleDown() {
    C.SendClick(2, 1)
}

func MiddleUp() {
    C.SendClick(2, 0)
}

func ScrollDown() {
    C.SendClick(5, 1)
    C.SendClick(5, 0)
}

func ScrollUp() {
    C.SendClick(4, 1)
    C.SendClick(4, 0)
}

func GoPrev() {
    C.SendClick(8, 1)
    C.SendClick(8, 0)
}

func GoNext() {
    C.SendClick(9, 1)
    C.SendClick(9, 0)
}
