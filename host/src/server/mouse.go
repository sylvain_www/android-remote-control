package server

import (
	"osimpl"
	"scratch"
)

const (
	mouseMove   = 1
	mouseLeft   = 2
	mouseMiddle = 3
	mouseRight  = 4
	leftDown    = 5
	leftUp      = 6
	rightDown   = 7
	rightUp     = 8
	middleDown  = 9
	middleUp    = 10
	scrollDown  = 11
	scrollUp    = 12
	goPrev      = 13
	goNext      = 14
)

func processMouse(message *scratch.ScratchMessage) {
	switch message.SecondaryCommand {
	case mouseMove:
		x, err := message.GetInt32()
		if err != nil {
			return
		}

		y, err := message.GetInt32()
		if err != nil {
			return
		}

		osimpl.MoveMouse(int(x), int(y))

	case mouseLeft:
		osimpl.LeftClick()

	case mouseMiddle:
		osimpl.MiddleClick()

	case mouseRight:
		osimpl.RightClick()

	case leftDown:
		osimpl.LeftDown()

	case leftUp:
		osimpl.LeftUp()

	case rightDown:
		osimpl.RightDown()

	case rightUp:
		osimpl.RightUp()

	case middleDown:
		osimpl.MiddleDown()

	case middleUp:
		osimpl.MiddleUp()

	case scrollDown:
		osimpl.ScrollDown()

	case scrollUp:
		osimpl.ScrollUp()

	case goPrev:
		osimpl.GoPrev()

	case goNext:
		osimpl.GoNext()
	}
}
