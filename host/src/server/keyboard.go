package server

import (
    "osimpl"
    "scratch"
)

const (
    keyCode   = 1
    keyDown = 2
    keyUp = 3
)

func processKeyboard(message *scratch.ScratchMessage) {
    switch message.SecondaryCommand {
    case keyCode:
        bte, err := message.GetInt32()
        if err == nil {
            osimpl.SendKeycode(int(bte))
        }
    case keyUp:
        bte, err := message.GetInt32()
        if err == nil {
            osimpl.SendKeyUp(int(bte))
        }
    case keyDown:
        bte, err := message.GetInt32()
        if err == nil {
            osimpl.SendKeyDown(int(bte))
        }
    }
}
