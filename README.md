PC remote control for Android
=============================

Android app and host server to control your PC running X windows from your telephone. The app provides a touchscreen mouse (with buttons), a full keyboard (typematrix layout) and hotkeys for most used functions.

![screenshot](http://bytebucket.org/sylvain_www/android-remote-control/raw/05deb9b70c4178ef96e9ff7dbeda05fe5e2eecef/screenshot.png)

You will most likely need to modify the keycodes if you want to use this tool on your setup. The app works by sending keycodes to a server on the host machine, which passes them to the X server unmodified.

I used this project to learn Android development, have mercy!

This project will only work for X windows environment and has not been tested on devices other than mine. That's why it is not released to the app market. The original project (link below) also provides servers for Windows and Mac, check it out!

This project is based on the [original work][id_link] from Phil Proctor (philliptproctor@gmail.com).

[id_link]: http://github.com/philproctor/SimpleComputerRemote
